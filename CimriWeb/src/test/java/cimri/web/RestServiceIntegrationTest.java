package cimri.web;

 

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.test.rule.OutputCapture;
import org.junit.Rule;
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RestServiceIntegrationTest {

	private static final Logger logger = LogManager
			.getLogger(RestServiceIntegrationTest .class);
	
	 
	@Rule
	public OutputCapture output = new OutputCapture();
	
	@Autowired
	private MockMvc mvc;
	
	 

	@Test
	public void test() throws Exception {
		 
		 logger.info("Integration Test ");
		 
		 this.mvc.perform(get("http://localhost:9595/results/snopy"))
			.andExpect(status().isOk()).andDo(print())
			.andExpect(MockMvcResultMatchers.jsonPath("Results").exists());
		 
	
	       
	       
		 
		 
		 
		
	}

}
