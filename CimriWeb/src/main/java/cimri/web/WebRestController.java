/*
Restful Servisler Xml Parse ve Json Sayfa oluşturma işlemlerini yapar 

örnek: Xml parse  http://localhost:9595/ParseXml/1
	   url arrayden 1 inci xmli parse eder 
 
*/
package cimri.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import cimri.web.model.CimriModel;
import cimri.web.model.dao.CimriDao;
import cimri.web.model.dao.DynamoDBDao;

@RestController
public class WebRestController {
	
	private static final String[] url = {"https://bitbucket.org/cimri/challenge/raw/3e019ced5866fa09ba63d8c08cb563abc976c0c8/site1.xml",
			"https://bitbucket.org/cimri/challenge/raw/3e019ced5866fa09ba63d8c08cb563abc976c0c8/site2.xml",
			"https://bitbucket.org/cimri/challenge/raw/3e019ced5866fa09ba63d8c08cb563abc976c0c8/site3.xml",
			"https://bitbucket.org/cimri/challenge/raw/3e019ced5866fa09ba63d8c08cb563abc976c0c8/site4.xml"};
	
	private static Logger log = LogManager.getLogger(WebRestController.class);
	
	
	@Autowired
	private ParseXml parse;
	@Autowired
	private CimriDao cimri;
	@Autowired
	private DynamoDBDao dynamoDBDao;
	
	@RequestMapping("/ParseXml/{urivar}")
	public String getXmlData(@PathVariable String urivar){
		
		int index=Integer.parseInt(urivar);
		
		try {
			 parse.parseBucketXml(url[index]);
			 parse.parseDynamoBucketXml();
			 
			 log.info("XML Dosyası DB ye Yazıldı");
			
		} catch (Exception e) {
			 log.error("Restful Servis ParseXml:"+e); 
		}
		
		return "istek Gönderildi";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/results/{urivar}", method=RequestMethod.GET, produces={"application/json","application/xml"})
	public void getResults(HttpServletResponse response,@PathVariable String urivar) throws IOException{
		
		JSONArray array = new JSONArray();
		JSONObject objectOuter = new JSONObject(); 
		JSONObject objectInner = new JSONObject(); 
		
		
		cimri.openSessionWithT();
		
		List<CimriModel> hmodel = cimri.findByTitle("%"+urivar+"%");		
		
		for(CimriModel ms:hmodel){
			
			objectInner = new JSONObject(); 
			objectInner.put("Title", ms.getTitle());
			objectInner.put("Brand", ms.getBrand());
			objectInner.put("Url", ms.getUrl());
			objectInner.put("Category", ms.getCategory());
			objectInner.put("Id", ms.getId());
			array.add(objectInner);
		}
		
		objectOuter.put("Results", array);
		
		cimri.closeSessionWithT();
		
		response.setContentType("application/json;charset=UTF-8");
	    response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().write(objectOuter.toJSONString());
		response.getWriter().flush();
		response.getWriter().close();
		
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/resultsdynamo/{urivar}", method=RequestMethod.GET, produces={"application/json","application/xml"})
	public void getDynamoResults(HttpServletResponse response,@PathVariable String urivar) throws IOException{
		
		JSONObject objectOuter = new JSONObject();  
		JSONObject objectInner = new JSONObject(); 
		
		String[] array1=dynamoDBDao.findById(urivar);
		
		objectInner.put("Dates",array1[0]);
		objectInner.put("Prices",array1[1]);
		objectOuter.put("Results", objectInner);
		
		response.setContentType("application/json;charset=UTF-8");
	    response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().write(objectOuter.toJSONString());
		response.getWriter().flush();
		response.getWriter().close();
	}
	 

}
