/*  
 * Conn Singleton Class file downloader ve content methodları içerir
 */


package cimri.web;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ConnectionDataSingleton {
	
	private List<String> cookies;
	private URLConnection conn;
	private final String USER_AGENT = "Mozilla/5.0";
	
	private static Logger log = LogManager.getLogger(ConnectionDataSingleton.class);

	private static ConnectionDataSingleton instance = new ConnectionDataSingleton();
	
	private ConnectionDataSingleton(){}
	
	public static ConnectionDataSingleton getInstance(){
	return instance;
	}
	
	public void GetPageContent(String url) throws Exception {

		URL obj = new URL(url);
		conn = (URLConnection) obj.openConnection();
		conn.setUseCaches(true);
		conn.setRequestProperty("User-Agent", USER_AGENT);
		conn.setRequestProperty("Accept",
								"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		conn.setConnectTimeout(3000*10);
		
		if (cookies != null) {
			for (String cookie : this.cookies) {
				conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
			}
		}
		 
	    int responseCode = 200;
	    log.info("Requet Alındı URL:"+url);
	    log.info("http Status Code"+responseCode);

		BufferedReader in = 
	            new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		 
		setCookies(conn.getHeaderFields().get("Set-Cookie"));
		DownloadPage(response.toString());

	  }
	  
	@SuppressWarnings("resource")
	public String GetPageContent() throws IOException{
		  
		StringBuilder sb = new StringBuilder();
		String myline;
		
		BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream("cimri.tmp"), "UTF8"));
					
			while((myline=br.readLine()) != null){ 
				  sb.append(myline);
			} 
			 return sb.toString();
	  }
	
	 
	
	  public void DownloadPage(String tmp){
		  
		  try {		   
	           
			  File myXml= new File("cimri.tmp");
	             
	           	if(!myXml.exists())myXml.createNewFile();
	              
	           BufferedWriter fw = new BufferedWriter(new OutputStreamWriter(
	        		    new FileOutputStream(myXml), "UTF-8"));
	           
	           fw.append(tmp);
	           fw.flush();
	           fw.close();
	           log.info("Dosya indirildi cimri.tmp dosyası oluşturuldu");
	     } catch (IOException e) {
	    	log.error("cimri.tmp dosyası yazılamadı:"+e);
	           
	     }
	  }
	  
	  public List<String> getCookies() {
		return cookies;
	  }

	  public void setCookies(List<String> cookies) {
		this.cookies = cookies;
	  }
	
	
}
