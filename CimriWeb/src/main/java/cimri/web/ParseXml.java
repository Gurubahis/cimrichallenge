/*

Class Mysql Server ve DynamoDB verileri yazar.

*/
package cimri.web;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cimri.web.model.CimriModel;
import cimri.web.model.DynamoDbModel;
import cimri.web.model.dao.CimriDao;
import cimri.web.model.dao.DynamoDBDao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;


 
@Component
public class ParseXml {
	
	private static String pageContent;
	
	@Autowired
	private CimriDao cimri;
	@Autowired 
	private DynamoDBDao dynamoDBDao;
	
	private static Logger log = LogManager.getLogger(WebRestController.class); 
	
	public boolean parseBucketXml(String url) throws Exception{
		
		ConnectionDataSingleton conn = ConnectionDataSingleton.getInstance(); 
		conn.GetPageContent(url);
	    setPageContent(conn.GetPageContent());
	     
	    
	    
	    JSONObject jsonObject = XML.toJSONObject(getPageContent());
	     
	    JSONObject rows = (JSONObject) jsonObject.get("rows");
	    JSONArray row= (JSONArray) rows.get("row");
	  
	    cimri.openSessionWithT();
	    
	    for (int index=0; index<row.length(); index++) {
	    	
	    	JSONObject innerObject = row.getJSONObject(index);
	    
	    	try{
	    		
	    		if(cimri.findById(innerObject.get("id").toString())==false)
	    		cimri.save(new CimriModel(innerObject.get("title").toString(),
	    							  innerObject.get("category").toString(),
	    							  innerObject.get("id").toString(),
	    							  innerObject.get("url").toString(),
	    							  innerObject.get("brand").toString()));
	    	}
	    	catch(Exception e){
	    		log.error(innerObject.get("id").toString()+"DB Hatası:"+e);

	    	}

	    }

	    cimri.closeSessionWithT();
	    	    	    
	    return true;
	}

	public boolean parseDynamoBucketXml() throws IOException {
		
		ConnectionDataSingleton conn = ConnectionDataSingleton.getInstance(); 
		setPageContent(conn.GetPageContent());
		
	    CookieHandler.setDefault(new CookieManager());
	    
	    try{
	    	dynamoDBDao.createTable();
	    	
	    }catch(Exception e){
	    	log.error("Dynamo DB Hata:"+e);
	    }
		
	    JSONObject jsonObject = XML.toJSONObject(getPageContent());
	    JSONObject rows = (JSONObject) jsonObject.get("rows");
	    JSONArray row= (JSONArray) rows.get("row");

	    for (int index=0; index<row.length(); index++) {
	    	
	    	JSONObject innerObject = row.getJSONObject(index);
	    	try{
	    		if(dynamoDBDao.isItemExist(innerObject.get("id").toString())==false)
	    		dynamoDBDao.save(new DynamoDbModel(innerObject.get("id").toString(),
	    										   innerObject.get("dates").toString(),
	    										   innerObject.get("prices").toString()));	    	
	    	
	    	}catch(Exception e){
	    	log.error(innerObject.get("id").toString()+"DynamoDb Kayıt Hatası:"+e);
	    	}
	    
	    }
	     

	return true;
	}
	
	public static String getPageContent() {
		return pageContent;
	}


	public static void setPageContent(String pageContent) {
		ParseXml.pageContent = pageContent;
	}
}
