package cimri.web.model.dao;

import java.util.List;

public interface CimriInterface<C> {

	public void save(C entity);
	
	public void update(C entity);
	
	public List<C> findByTitle(String title);
	
	public boolean findById(String id);
	
	public void delete(C entity);
	
	public List<C> findAll();
	
	public void deleteAll();
	
}