/*

Mysql Dao Concrete Class

*/
package cimri.web.model.dao;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.hibernate.Criteria;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.stereotype.Repository;

import cimri.web.model.CimriModel;
 

@SuppressWarnings("deprecation")
@Transactional
@Repository
public class CimriDao implements CimriInterface<CimriModel>{
	
	private StatelessSession cSession;
	private Transaction t; 
 
	public CimriDao(){}
	
	public StatelessSession openSession(){
		
		cSession=HibernateUtility.getSessionFactory().openStatelessSession();
		return cSession;
	}
	
	public StatelessSession openSessionWithT(){
		
		cSession=HibernateUtility.getSessionFactory().openStatelessSession();
		t=cSession.beginTransaction();
		return cSession;
	}
	
	public void closeSession(){
		cSession.close();
	}
	
	public void closeSessionWithT(){
		try{
		t.commit();
		 
		}catch(ConstraintViolationException e) {
		    System.out.println(e);
		     
		} catch(JDBCConnectionException e) {
		    System.out.println(e);
		    
		}
		catch(Exception e){
			 System.out.println(e);
		} 
		finally{
			cSession.close();
		}
		
	}
	
	 

	public StatelessSession getcSession() {
		return cSession;
	}

	public void setcSession(StatelessSession cSession) {
		this.cSession = cSession;
	}

	@Override
	public void save(CimriModel entity) {
		try{
		 getcSession().insert(entity);
		}catch(ConstraintViolationException e) {	 
		    System.out.println(e);
		} catch(JDBCConnectionException e) {
		    System.out.println(e);  
		}catch(Exception e){
			 System.out.println(e);
		} 
	}

	@Override
	public void update(CimriModel entity) {
		getcSession().update(entity);
		
	}

	@Override
	public List<CimriModel> findByTitle(String title) {
		 
			 
			    
		Criteria criteria = getcSession().createCriteria(CimriModel.class);
		criteria.add(Expression.like("title",title));
		@SuppressWarnings("unchecked")
		List<CimriModel>cimri =criteria.list();
		return cimri;
		 
	} 
	
	
	@Override
	public boolean findById(String id) {
		
		boolean isRecordExist=false;
		isRecordExist=getcSession().get(CimriModel.class, id) ==null ? false : true;
		  
		return isRecordExist;
	}
	 
	@Override
	public void delete(CimriModel entity) {
		getcSession().delete(entity);
		
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<CimriModel> findAll() {
		List<CimriModel> cList = (List<CimriModel>) getcSession().createQuery("from Cimri").list();
		return cList;
	}

	@Override
	public void deleteAll() {
		 List<CimriModel> cList=findAll();
		 for(CimriModel m : cList){
			 delete(m);
		 }
		
	}



	

	 

}
