package cimri.web.model.dao;

public interface DynamoDBInterface<C> {
	public void save(C entity);
	public String[] findById(String id);
	public boolean isItemExist(String id);
	public void createTable();
}
