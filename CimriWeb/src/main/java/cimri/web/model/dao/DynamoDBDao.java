/*
 * Dynamo DB Dao Concrete Class
 */
package cimri.web.model.dao;

import java.util.Arrays;
import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import cimri.web.model.DynamoDbModel;


@Repository
public class DynamoDBDao implements DynamoDBInterface<DynamoDbModel> {

	private DynamoDB dynamoDB;
	
	private static Logger log = LogManager.getLogger(DynamoDBDao.class);
	
	public DynamoDBDao(){
		 DynamoSingleton dyn = DynamoSingleton.getInstance();
		 try {
			setDynamoDB(dyn.GetConn("http://localhost:8000"));
		} catch (Exception e) {
			 
			log.error("DynamoDB connection hatası:"+e);
		}
		
	}
	
	@Override
	public void save(DynamoDbModel entity) {
		
	        Table table = getDynamoDB().getTable("cimridb");
	        
	        try {
	        	if(!entity.getId().isEmpty() && !entity.getDates().isEmpty()
	        								 && !entity.getPrices().isEmpty()){
	            
	        		 table.putItem(new Item()
	        				 .withPrimaryKey("DynamoId",entity.getId(),"id", entity.getId())
	        				 .withString("dates", entity.getDates())
	        				 .withString("prices", entity.getPrices())); 
	        	}
	        } catch (Exception e) {
	            
	            log.error("Item insert hatası:"+e);
	             
	        }       
		
	}

	@Override
	public String[] findById(String id) {
		 
		String[] array = {"",""};		 
			
			Table table = getDynamoDB().getTable("cimridb");
	        
			ScanSpec scanSpec = new ScanSpec().withFilterExpression("id = :v_id")
		            			.withValueMap(new ValueMap()
		            			.withString(":v_id", id)); 
	         
			ItemCollection<ScanOutcome> items = table.scan(scanSpec);
	        Iterator<Item> iterator = items.iterator();
	        JSONObject json = null ;
	        
	        while (iterator.hasNext()) {
	    	
	        	json = new JSONObject(iterator.next().toJSONPretty()); 
	        	 array[0]=json.get("dates").toString();
	    	     array[1]=json.get("prices").toString();
	        }  
	        
	        
	    
	        
		 
	     return array;
	}

	 
 

	@Override
	public void createTable() {
		  
	   String tableName = "cimridb";
	        
	   try {
	          getDynamoDB().createTable(tableName,
	        		  		Arrays.asList(new KeySchemaElement("id", KeyType.HASH)),
	        		  		Arrays.asList(new AttributeDefinition("id", ScalarAttributeType.S)), 
	        		  		new ProvisionedThroughput(10L, 10L));
	       
	   }catch(AmazonClientException e){
	        	log.error("Dynamo DB Tablo Hatası:"+e);
	   }
	}
	
	@Override
	public boolean isItemExist(String id) {
	
			Table table = getDynamoDB().getTable("cimridb");
	        
	        ScanSpec scanSpec = new ScanSpec().withFilterExpression("id = :v_id")
		            		.withValueMap(new ValueMap()
			                .withString(":v_id", id));
	        
	         ItemCollection<ScanOutcome> items = table.scan(scanSpec);
	         Iterator<Item> iterator = items.iterator();
 
	         return iterator.hasNext();
	}

	public DynamoDB getDynamoDB() {
		return dynamoDB;
	}
	public void setDynamoDB(DynamoDB dynamoDB) {
		this.dynamoDB = dynamoDB;
	}

}
