/*
 * Dynamo DB conn Singelton Class
 */
package cimri.web.model.dao;



import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;


public class DynamoSingleton {
 

	private static DynamoSingleton instance = new DynamoSingleton();
	
	private DynamoSingleton(){}
	
	public static DynamoSingleton getInstance(){return instance;}
	
	public DynamoDB GetConn(String endPoint) throws Exception {
 
		AmazonDynamoDBClient client = new AmazonDynamoDBClient();
        client.setEndpoint(endPoint);
        DynamoDB dynamoDB = new DynamoDB(client);
		
		return dynamoDB;

	  }
	
	
}
