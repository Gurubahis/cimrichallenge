/*
 * Dynamo DB Model Class
 */
package cimri.web.model;

public class DynamoDbModel {

public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDates() {
		return dates;
	}
	public void setDates(String dates) {
		this.dates = dates;
	}
	public String getPrices() {
		return prices;
	}
	public void setPrices(String prices) {
		this.prices = prices;
	}
private String id;
private String dates;
private String prices;

public DynamoDbModel(String id, String dates, String prices) {
	this.id=id;
	this.dates=dates;
	this.prices=prices;
}
public DynamoDbModel() {}

}
