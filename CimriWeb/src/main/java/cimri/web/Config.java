/*
 * Config Class ta Beanler register ediliyor
 */

package cimri.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import cimri.web.model.dao.CimriDao;
import cimri.web.model.dao.DynamoDBDao;

@Configuration
public class Config {
	@Bean
	public ParseXml parseXml(){
		return new ParseXml();
	}
	@Bean 
	public CimriDao cimriDao(){
		return new CimriDao();
	}
	@Bean 
	public DynamoDBDao dynamoDBDao(){
		return new DynamoDBDao();
	}
	
}

