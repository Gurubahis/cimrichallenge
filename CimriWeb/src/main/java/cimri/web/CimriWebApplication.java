package cimri.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CimriWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(CimriWebApplication.class, args);
	}
}
